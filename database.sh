#!/bin/bash

docker run --name some-postgres -e POSTGRES_USER=db_user -e POSTGRES_PASSWORD=db_pass -e POSTGRES_DB=db_name -p 5432:5432 -d postgres
