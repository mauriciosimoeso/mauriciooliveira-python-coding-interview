from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


class Student(AbstractUser):

    id = models.AutoField(primary_key=True)
    enrollment = models.BooleanField(default=False)
    models.DateTimeField

    subscribed_to = models.ManyToManyField(to='subjects.Subject')

    def pre_save(self, model_instance, add):
        self.enrollment = self.subscribed_to.count() >= 7
