from django.shortcuts import render
from rest_framework import viewsets, mixins
from students.models import Student
from students import serializers

# Create your views here.


class StudentView(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin):

    queryset = Student.objects.all()
    serializer_class = serializers.StudentSerializer
