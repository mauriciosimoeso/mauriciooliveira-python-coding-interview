from django.contrib import admin
from students.models import Student
from django.contrib.auth.admin import UserAdmin
# Register your models here.

admin.register(Student, UserAdmin)


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    fields = ('subscribed_to', 'enrollment')
