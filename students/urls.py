from django.urls import include, path
from rest_framework import routers
from students import views

router = routers.DefaultRouter()
router.register(r'students', views.StudentView)

urlpatterns = [
    path('api/', include(router.urls)),
]
